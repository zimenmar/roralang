I don't expect anybody to understand these in depth. I just want to show I've thought about it, done some work and ain't lazy.

* umožnit operátor `:`, který je aplikace, ale naopak
* možná mít klasický směr aplikace ale umožnit right aplikaci pomocí `|`
* copy operátor
* zakázat apply napříč řádky
* mít #Tag jako další unarní operátor
* umožnit trailing ;,
* one-element tuple
* dořešit ref
* možná zrušit | a &, protože to už dělá ,
* nerozlišovat tuply a list?
* ! by mohlo negovat i funkce
* změnit Unmatched na True/False -> pak lépe vyniká ,
* princip všechno je funkce
* zakázat aplikace přes řádky
* rozdvojovací operátor a slučovací operátor (vlastně možná lift operátor)
    * rozdvojovací operátor může nahradit `==>` -- neřeší tohle trochu nefunkční pattern matching na tuply?
* rozmyslet currying (Fun append:(Int i -> List l -> ...); #Nil append:7 append:8 append:9)
    * jinak nefunguje ani v jednom směru
        * a -> b -> c -> ...
        * (c (b (a f)))
        * (f:a):b):c
    * změnit asociativitu pro `:`? Co to rozbije?
    * nevyřadit tím tuply? Dávají silné constraints na comma operator
* změnit @v a @^ na a =>@ @=>
* obávám se, že aktuálně nefunguje pattern matching na tuply -- vnější join tuplu rozdělí a dovnitř se nedostane celá
    * operátor, který rozdvojí tak, že vezme head z tuplu
* operátor, který vezme něco, odignoruje to a vrátí konstantu
* chci, aby většina operátorů fungovala i na funkcích - jak?
    * rozdvojit parametr
    * rozdělit tuple
    * tuply rozdělit, jinak rozdvojit
    * nerozbije to něco?
    * TBH moc implementační práce -- chaining funkcí je snažší a pokud chci chainit binární funkce, tak to holt udělám přes proměnnou
* problém s ukládáním funkce do proměnné (vyrobí to schainěnou funkci) by se dal řešit také podle contextu a ne dělením apply a rapply
* možná by to chtělo znovu přehodnotit precendenci (hlavně ",")
* nepoouštím binárními "|" a "&" celý princip róry? Je vážně tak skvělý, když mám potřebu ho opouštět?

* myšlenka z growing a language je pěkná, ale je k ničemu na toy language
    * šlo by umožnit custom operátory tím, že bych prostě tokenizoval operátory
    * otázka, jestli to tady stojí za to
    * v rámci systematizace a umožnění by bylo zajímavé mít všechny binop jako obyčejné identifikátory
        * v `a + b` by tak `a +` vyrobilo funkci
        * případně vynutit `a +: b`

* jakože fakt by se hodilo @@ pro přístup k outer loopu
* kdyby to byl high-level jazyk, tak by se hodně hodily eta konverze
* ^(a ; b) Co s tím?
* a ; b & c ; d je fakt divné
* fakt by to chtělo umět operátor, který předělá konstantu na funkci, která ignoruje input a erátí konstantu
* chybí mi komponenta, která udělá tohle: pokud je podmínka splněná, na argument zavolej tuto funkci
    * možná by se na této komponentě celý jazyk založil lépe
* možná jednotnější a univerzálnější by byla jednotka, která si může brát z vnějšího streamu a dávat do něj

* když jsem odstranil conditional funkci, tak jsem ztratil to, že můžu hezky vylít obsah #Tagu do scopu
* ty středníky jsou trochu matoucí -- ale člověk si zvykne ( možná prostě by default neaplikovat, dát operátor na aplikaci, rozhodovat o aplikování loopu na základě toho, jestli bere vstup) (nebo prostě pokud nejde aplikovat, tak přepsat)
* možná dělat indexaci konstantou?
* možná dát místo $ ^ (hezky to referuje na předchozí věc)
* jako globální proměnné nejsou hezké; já vím; co takhle jazyk, ve kterém by byly nějak zabudované monády
* having to write @< at the end of loop is annoying as fuck
* It is sooo tempting to extend "\" notation to allow accepting whole rora - it would be like lambda e.g. `\(^ * 5)` however, I'd need closures which are extra work to add to compiler
    * Pokud už přidám closures, nedává smysl rozlišovat syntax pro top-level a jiné funkce
* Chtěl bych umět zapisovat do structu v róře jako `Y(value => ^.attr)` -- indexer není dostatečně general (celkově jsem měl být asi schopný evalovalt term i mimo róru
* Chtělo by to generika i pro structy (třeba Src)
* Stále mám trochu pocit, že @ operace jsou slabé (bylo by fajn mít možnost přistoupit jak k yieldlist, tak k break), bylo by fajn mít možnost rozlišit, jestli se breaknulo kvůli EOL nebo kvůli manuálnímu breaku
* Break vygenerovaný ze zdroje řádu 2 je, zdá se, příliš silný. Vnitřní loop by ho chtěl nějak handlovat
    * Potenciálně by šlo zachovat oba tím, že by šel escapovat (`\(=>@)`)
* Řešením obou výše zmíněných problémů by mohlo být, že pokud break vrací non() hodnotu, tak se vrátí tuple (generator, breakval)
* Super by bylo umět peaknout na zdroj nebo umět vrátit na zpátky do zdroje
* Pro parsování by bylo mnohem jednodušší mít signaturu `(a, b) | f | rv`
* $ -> .
* možná by LogOps měli být nad assignmentem `5 > 3  &  true =>var`
* ^ by mohlo ukazovat na nejbližší středník a ne (

# Kterým směrem má jít aplikace

* pravý směr umožňuje mít \*2 jako funkci
    * to ale neumožňuje precedenci
    * zase to umožňuje hezké conditions
    * nejlepší by bylo v nějaké podobě oboje, ale to je moc práce
