[String] | remove_nops | [String] [ 
    =>@
    ( (^^, ' ') split $0 ( ^ == "NOP"  ?  @<  :  ())
    ; ^ @=>
    )
    @<    
]

// This assumes that all instructions have no side effects and can be safely removed
// This false assumption doesn't manifest itself
// GETCHAR is always later tested for 0 so it is always used
// flags from arithmetics is never used (only in CMP)
// LOOP and CALL is handled separately
// jumps are not used with registers
[String]: program, [[Int]]: live | remove_dead_ass | [String] (
    program enumerate
    [ =>@ (^$0 => Int:i; ^$1)
      ( (^^, ' ') split $0 (^ == "LOOP" | ^ == "CALL")  ?  ^ @=> @<  : ^)
      (^ get_kills len != 1  ?  ^ @=> @<  : ^)
      (^ get_kills $0 (^, live$(i+1)) elem_int  
      ?  ^ @=>
      :  "NOP" @=>
      )
      @<
    ]
)

                 
