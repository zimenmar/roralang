[Token]: token_src
Int: src_idx
Int: src_len

// There is a common problem -- I read a last token and store it in a
// variable. Then I read the next token which triggers "break" because
// the list ended. However the token in variable is never emmited. I
// deal with it in almost every parse. Possible solution would be to add
// multiple empty roras at the end of each rora. This way, these noops
// would "flush" the system. I thought of it too late

[Token] | set_src | () (
    ; ^ => token_src
    ; ^ len => src_len
    ; 0 => src_idx
)

() | get_src | Ast (
    src_idx == src_len
        ? #Failed{}
        : #Token{ token_src$(src_idx Y((^^+1) => src_idx)) => t }
)

() | parse_paren | Ast (
   ; ' ' => Int: last_token
   ; [ get_src ( ^ #Failed  ?  @> : ^.t)
       (^.str$0 ( (^^, "({[") elem_int  ?  parse_paren @=>
                : (^^, ")}]") elem_int  ?  ^ => last_token; @>
                : #Token{ ^^^ => t } @=>
                )
       ) @<
     ] ( last_token == ')'  ?  #Paren { #Rora{ ^^^ => terms } => pbody }
       : last_token == ']'  ?  #Loop  { #Rora{ ^^^ => terms } => lbody }
       : last_token == '}'  ?  #Struct{ #Rora{ ^^^ => terms } => sbody 
                                      , "" => struct_tag
                                      }
       :                       #Rora{ ^^ => terms}
       )
)

[Ast], [String]:splitter | rora_split | [Ast] (
  ^$0 [
      ; #Failed{} =>Ast:last
      ; [ =>@@ ( ^ #Token & (^^.t.str, splitter) elem_str
               ? ^ =>last; @>
               : ^ @=>
               ) 
        @<
        ] 
        #Rora{ ^ => terms}
        @=>
      ; last #Failed  ?  @>  : last @=> @<
      ]
)

[Ast] | split_ass | [Ast] [
      ; #Failed{} =>Ast:last
      ; [ =>@@ ( ^ #Ass
               ? ^ =>last; @>
               : ^ @=>
               ) 
        @<
        ] 
        #Rora{ ^ => terms}
        @=>
      ; last #Failed  ?  @>  : last @=> @<
]

[Ast] | parse_defs | [Ast] (
  ^ [ ; #Failed{} =>Ast:arrow
      ; =>@ ( ^ #Token & ^.t.str == "=>"
            ? ; ^ =>arrow
              ; =>@ =>Ast:type
                ( ^ #Token & ^.t.str$0 >= 'a' & ^.t.str$0 =< 'z'
                ? ; arrow @=>
                  ; type @=>
                  ; @<
                : ()
                )
              ; =>@ ( ^ #Token & ^.t.str == ":"
                    ? ; #Ass{} @=>
                      ; #VarDef{ =>@ .t .str => var_name
                             ; type => var_type
                             } @=>
                    : ; arrow @=>
                      ; type @=>
                      ; ^@=>
                    )
            : ^ @=>
            ) 
      ; @<
    ]
    ( arrow #Failed
    ? ^
    : [^^, [arrow, type]] flatten
    )
)

[Ast] | parse_vardef | [Ast] (
    ; false => Bool:ignore_rora
    ; ^ [ =>@ ( ^ #Token & ^.t.str == "?"  
              ? true => ignore_rora
              : @<
              )]
    ; ignore_rora 
      ? ^
      : ^ [ ; =>@ =>Ast:last
            ; [ =>@@ ( ^ #Token & ^.t.str == ":"
                     ? ; #VarDef{ =>@@ .t .str => var_name
                                ; last => var_type
                                } @@=>
                       ; #Rora{ [] => terms } => last // The next line can fail and empty rora is noop
                       ; =>@@ =>last
                     : ; last @@=>
                       ; ^ =>last
                     ) @<
              ]
            ; last @=>
          ]
)

[Ast] | parse_ternary | [Ast] (
    ; (^^, ["?", ":"]) rora_split reverse [
          ; =>@ =>Ast:else
          [ ; =>@@ // :
            ; =>@@ =>Ast:then
            ; =>@@ // ?
            ; =>@@ =>Ast:cond
            ; #If{ cond=>cond ; then=>then ; else=>else } => else
            ; @<
          ]
      ]
    ; [else]
    )

[Ast], String:splitter  | parse_split | [Ast] (
 ; false =>Bool:is_match
 ; (^^$0, [splitter]) rora_split
   [ =>@ (^ #Rora ? ^ @=> : true =>is_match ) @<]
   (is_match
   ? ^
     ( splitter == ","  ?  #Tuple{ ^^ => tuplees }
     : splitter == "|"  ?  #LogOp{ ^^ => ops, false => is_and }
     : splitter == "&"  ?  #LogOp{ ^^ => ops, true => is_and }
     : #Failed{}
     )
     [^]
   : ^
   )
)

[Ast], [String]:splitters | parse_binop | [Ast] (
    ^ rora_split 
    [ ; =>@ =>Ast:acc
      ; [ #BinOp{ acc =>l =>@@ .t .str =>op; =>@@ =>r} =>acc @< ]
      ; acc
    ]
)

[Ast] | parse_misc | [Ast] [
    =>@ ( ^ #Token !  ?  ^
        : ^.t.str ( ^ == "Y"  ?  #Fork{ =>@ .pbody =>fbody }
                  : ^ == "."  ?  #Attr{ =>@ .t.str =>attr }
                  : ^ == "$"  ?  =>@
                                 [^]
                                 #Rora{ ^ => terms }
                                 #Index{ ^ => index }
                  : ^ == "#"  ?  #Tag{ =>@ .t.str =>tagname }
                  :              ^^
                  )
        ) @=> @<
]

[Ast] | parse_ass | [Ast] (
  ^
  [ ; =>@ =>Ast: last
    ;[ ( last #Token ! | last.t.str != "=>"
       ? ; last @@=>
         ; =>@@ =>last
       : ; =>@@ =>last
         ; #Ass{} @@=>
         ; [ ; last @=>
             ; #Rora{ [] => terms } =>last
             ; =>@@@ =>last 
             ; last ( ^ #Index | ^ #Attr  ?  @<  : @> )
           ]
           #Assign { ^ reverse tail reverse 
                     #Rora { ^ => terms } 
                     => assignee 
                   ; ^$(^ len - 1)
                     (^ #Token  ?  #Name{ ^^.t.str => name}  :  ^)
                     => assigner 
                   }
           @@=>
       ) @< 
      ]
  ]
  split_ass
)

Ast | parse_toplevel | Ast (
    ^.terms 
    [ ; =>@ => Ast:last
      ; [ =>@@ 
          Y ( last #Token & last.t.str == ";"  ?  ^ =>last @<  : ())
          Y ( ^ #Token & ^.t.str == ";"  ?  @<  : ())
          ( ^ #Token ! | ^.t.str != "|" & ^.t.str != ","
          ? ( last #VarDef
            ? ; last @@=>
              ; ^^ =>last
            : ; #StructDef{ last.t.str => struct_name
                          ; ^^^ => struct_def
                          } @@=>
              ; =>@@ =>last
            )
          : ; [ ; last @=>
                ; ^^ =>last
                ; [ last #Token & last.t.str == "|"
                  ?   @>
                  : ; =>@@@@ @@=>
                    ; =>@@@@ =>last
                    ; @<
                  ]
              ]
              #RoraDef{ ^ => args
                      ; =>@@ .t.str => rora_name
                      ; =>@@
                      ; =>@@ => rv
                      ; =>@@ => body
                      } @@=>
            ; =>@@ =>last
          ) @<
        ]
    ]
    #Rora{ ^ => terms }
)

[Ast] | parse_terms | [Ast] [ =>@
    ( ^ #Token !  ?  ^ @=> @<  :  ^.t )
    ( ^ #Hat  ?  #Src { ^^.str len => power } @=> @<
    : ^ #Num  ?  #Lit { ^^.str str_to_int => lit } @=> @<
    : ^.str
    )
    ( ('@', ^^) elem_int  ?  ^ ( 
        ; ^ [ =>@ (^ == '@' ? ^ @=> : ()) @< ] len => Int:pow
        ; ^ ( ^$0 == '='           ?  #Pop{ pow => popower }
            : ^$(^ len -2) == '='  ?  #Push{ pow => pupower }
            : ^$(^ len -1) == '<'  ?  #Continue { pow => cpower }
            : ^$(^ len -1) == '>'  ?  #Break { pow => bpower }
            : #Failed{}
            )
        )
    : ^$0 == '''     ?  ^$1 #Lit{ ^ => lit }
    : ^$0 == '"'     ?  ^ tail reverse tail reverse
                        [ =>@ #Lit{ ^ => lit} @=> @<]
                        #Tuple{ ^ => tuplees }
                        #Rora{ [^^] => terms }
                        #Loop{ ^ => lbody }
    : ^ == ";"       ?  #Stop{}
    : ^ == "!"       ?  #Not{}
    : ^ == "stdin"   ?  #GetChar{}
    : ^ == "stdout"  ?  #PutChar{}
    : ^ == "stderr"  ?  #PutChar{}
    : ^ == "putnum"  ?  #PutNum{}
    : ^ == "true"    ?  #BoolLit{ 1 => bool_lit }
    : ^ == "false"   ?  #BoolLit{ 0 => bool_lit }
    : #Name{ ^^ => name }
    )
    @=>
    @<
]

[Ast] | parse_tagged_struct | [Ast] [
  ; (^^ len == 0  ?  [] @>  :  ())
  ; ^
    [ =>@
      ( ^ #Tag !  ?  ^ @=> @<  : ^)
      ( =>@ ( ^ #Struct  
            ? ; ^^.tagname => ^.struct_tag
              ; ^ @=>
            : ; ^^ @=>
              ; ^ @=>
            )
      )
      @<
    ]
    ( ^^$(^ len - 1)
      ( ^ #Tag
      ? [^^^, [^^^]] flatten
      : ^^
      )
    )
    @>
]

Ast:ast | get_roras | [Ast] (
    ^ get_children [ =>@ get_roras @=> @< ] 
    flatten
    (ast #Rora  ? [^^, [ast]] flatten : ^)
)

[Ast] | flatten_rora | [Ast] [
    =>@ ( ^ #Rora ? ^.terms [ =>@ @@=> @< ]
        : ^ #Ass  ? ()
        : ^ @=> 
        ) @<
]

[Ast] | squash_rora | [Ast] 
    ( ^ len == 1 & ^$0 #Rora
    ? ^$0.terms
    : ^
    )

[Token] | parse | Ast (
    ^ set_src
    parse_paren
    Y( ^ get_roras [ =>@ =>Ast:r .terms parse_defs => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms parse_vardef => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms parse_ternary => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms squash_rora => r.terms @< ])
    parse_toplevel
    Y( ^ get_roras [ =>@ =>Ast:r .terms parse_misc => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms parse_ass => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms (^, ",") parse_split => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms (^, "|") parse_split => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms (^, "&") parse_split => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms squash_rora => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms (^, ["==", "!=", "<", ">", "=<", ">="]) parse_binop => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms (^, ["+", "-"]) parse_binop => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms (^, ["%", "/", "*"]) parse_binop => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms squash_rora => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms flatten_rora => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms squash_rora => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms parse_terms => r.terms @< ])
    Y( ^ get_roras [ =>@ =>Ast:r .terms parse_tagged_struct => r.terms @< ])
)

