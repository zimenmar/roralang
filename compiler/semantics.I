[(String, Ast)]: names
[(String, [(String, (Int, Type))])]: structs
[(String, (String, Int))]: tags

String | get_struct_size | Int (
  ; 0 => Int: rv
  ; (structs, ^^) get
    [ (=>@ $1 $0 + 1, rv) max => rv @< ]
  ; rv
)

() | print_struct_analysis | () (
  ; tags [ =>@ 
           ["\t", ^$0, " ", ^$1$0, " ", ^$1$1 int_to_str] flatten 
           println_err
           @<
         ]
  ; structs [ =>@
              Y("\t" print_err)
              Y(^$0 println_err)
              $1
              [ =>@
                [ "\t\t", ^$0
                , " ", ^$1$0 int_to_str
                , " ", ^$1$1 type_to_str
                ] flatten
                println_err
                @<
              ]
              @<
            ]
)

Int: cur_tag_id
Int: cur_struct_name

Ast | resolve_toplevel | () 
    ( ^ #StructDef  ?  ; 0 => cur_tag_id
                       ; ^.struct_name => cur_struct_name
                       ; ^.struct_def
                         resolve_struct 
                         [ [("tag", (0, #NamedType{ "Int" => name }))]
                         , ^
                         ]
                         join_fields
                         (structs, ^^.struct_name, ^) set => structs
    : ^ #VarDef     ?  ; (names, ^^.var_name, ^^) set => names
                       ; true => ^.is_global
    : ^ #RoraDef    ?  (names, ^^.rora_name, ^^) set => names
    : ^ #Rora       ?  ^.terms [ =>@ resolve_toplevel @< ]
    : ()
    )

String | mark_tag | () (
    ^ == ""
    ? ()
    : cur_tag_id
      Y(^ + 1 => cur_tag_id)
      (cur_struct_name, ^)
      (tags, ^^, ^) set => tags
)

[[(String, (Int, Type))]] | join_fields | [(String, (Int, Type))] (
  ; 0 => Int: offset
  ; 0 => Int: max_suboffset
  ; ^ [ ; =>@ 
          [ =>@ (^$0
                , ^$1 ( ^$0 
                        Y((^^ + 1, max_suboffset) max => max_suboffset)  
                        + offset
                      , ^$1
                      )
                ) @@=> @< ]
        ; offset + max_suboffset => offset
        ; @<
      ]
  )

Ast | resolve_struct | [(String, (Int, Type))]
    ( ^ #Paren  ?  ^.pbody resolve_struct
    : ^ #Rora   ?  ( ^^.terms len == 0  
                   ?  []
                   :  ^^.terms$0 resolve_struct
                   )
    : ^ #Struct ?  ; ^.struct_tag mark_tag
                   ; ^.sbody resolve_struct
    : ^ #VarDef ?  [(^^^.var_name, (0, ^^^^.var_type ast_to_type))]
    : ^ #LogOp & ^.is_and !  ?  ^.ops [ =>@ resolve_struct @=> @< ] flatten
    : ^ #LogOp  ?  ^.ops [ =>@ resolve_struct @=> @< ] join_fields
    : ^ #Tuple  ?  ^.tuplees [ =>@ resolve_struct @=> @< ] join_fields
    : []
    )
        

Ast | resolve_fun | () 
    ( ^ #VarDef  ?  ; (names, ^^.var_name, ^^) set => names
                    ; false => ^.is_global
    : ^ #Name    ?  (names, ^^.name) get => ^.target
    : ^ get_children [ =>@ resolve_fun @< ]
    )

Ast | resolve_names | () (
  ; ^ resolve_toplevel
  ; ^.terms [ =>@ (^ #RoraDef  ?  ^.body resolve_fun  : ()) @< ]
)

Int | gen_src_vardef | Ast (
    ^
    ["src", ^ int_to_str] flatten
    #VarDef{ #Rora{[] => terms} => var_type
           , ^ => var_name
           }
)

Ast | resolve_src | () (
  ; ^ id_ast
  ; ^.terms [ =>@ (^ #RoraDef  ?  (^^.body, []) _resolve_src  :  ()) @< ]
  ; ^ id_ast
)
Ast, [Ast]:stack | _resolve_src | () ( ^$0
    ( ^ #Paren   ?  (^^.pbody, [stack, [^^^^]] flatten) _resolve_src
    : ^ #Struct  ?  (^^.sbody, [stack, [^^^^]] flatten) _resolve_src
    : ^ #Loop    ?  (^^.lbody, [stack, [^^^^]] flatten) _resolve_src
    : ^ #Fork    ?  (^^.fbody, [stack, [^^^^]] flatten) _resolve_src
    : ^ #Index 
    & ^.index #Rora 
    & ^.index.terms$0 #Paren  
                 ?  (^^.index.terms$0, stack) _resolve_src
    : ^ #Assign 
    & ^.assigner #Index 
    & ^.assigner.index #Rora 
    & ^.assigner.index.terms$0 #Paren
                 ?  (^^.assigner.index.terms$0, stack) _resolve_src
    : ^ #Rora    ?  ^.terms [ 
        =>@
        Y((^^, stack) _resolve_src)
        ( ^ #Src     ? stack$(^ len - ^^.power).id 
                       ["src", ^ int_to_str] flatten
                       #Name{ ^ => name }
                       @=>
        : ^ #Index 
        & ^.index #Rora 
        & ^.index.terms$0 #Paren
                     ?  ; ^.index.terms$0.id 
                          gen_src_vardef 
                          @=> 
                        ; ^ @=>
        : ^ #Assign 
        & ^.assigner #Index 
        & ^.assigner.index #Rora 
        & ^.assigner.index.terms$0 #Paren
                     ?  ; ^.assigner.index.terms$0.id
                          gen_src_vardef
                          [^^.assignee.terms, [^^]] flatten
                          => ^.assignee.terms
                        ; ^ @=>
        : ^ #Paren   ?  ^.id gen_src_vardef @=> ; ^ @=>
        : ^ #Struct  ?  ^.id gen_src_vardef @=> ; ^ @=>
        : ^ #Paren   ?  ^.id gen_src_vardef @=> ; ^ @=>
        : ^ #Loop    ?  ^.id gen_src_vardef @=> ; ^ @=>
        : ^ #Fork    ?  ^.id gen_src_vardef @=> ; ^ @=>
        : ^ @=>
        ) @< ] => ^.terms
    : ^ #VarDef  ? ()
    : ^ get_children [ =>@ (^, stack) _resolve_src @< ]
    )
)

Ast | resolve_loopop | () (
  ; (^^, []) _resolve_loopop
)

Ast, [Ast]:stack | _resolve_loopop | () ( ^$0
    ( ^ #Loop      ?  ; false => ^.is_generator
                      ; (^^.lbody, [stack, [^^^^]] flatten) _resolve_loopop
    : ^ #Continue  ?  stack$(^ len - ^^.cpower) => ^.cloop
    : ^ #Break     ?  stack$(^ len - ^^.bpower) => ^.bloop
    : ^ #GetChar   ?  stack$(^ len - 1) => ^.gloop
    : ^ #Push      ?  ; stack$(^ len - ^^.pupower) => ^.puloop
                      ; true => stack$(^ len - ^^.pupower).is_generator
    : ^ #Pop       ?  ; stack$(^ len - ^^.popower) => ^.poloop
                      ; stack$(^ len - 1) => ^.break_target
    : ^ get_children [ (=>@, stack) _resolve_loopop @< ]
    )
)

Ast | resolve_named_args | () 
    ( ^ #RoraDef  ?  ; (^^.args len == 1) => Bool:is_singleton
                     ; ^.args enumerate 
                       [ ; =>@ ( ^$1 #VarDef  
                               ?  ; ^$0 => Int:index
                                  ; ^$1 => Ast:var_def
                               :  @<
                               )
                         ; [ #Src{ 1 => power } [^]
                           , ( is_singleton
                             ? []
                             : #Lit{ index => lit }
                               [^]
                               #Rora{ ^ => terms }
                               #Index{ ^ => index}
                               [^]
                             )
                           , #VarDef{ var_def.var_name => var_name 
                                    , #Rora{ [] => terms } => var_type
                                    } [^]
                           ] flatten
                           #Rora{ ^ => terms}
                           #Fork{ ^ => fbody}
                           @=>
                         ; @<
                       ]
                       [^, [^^^.body]] flatten
                       #Rora{ ^ => terms }
                       => ^.body
    : ^ get_children [ =>@ resolve_named_args @< ]
    )

Ast | resolve_struct_creation | () (
    ; ^ id_ast
    ; (^^, "") _resolve_struct_creation
    ; ^ id_ast
)

Ast, String:id | _resolve_struct_creation | Ast ( ^$0
    ( ^ #StructDef ?  ^
    : ^ #Struct    ?  ; ^.id ["struct", ^ int_to_str] flatten => String: new_id
                      ; [ #Struct{ #Rora{ [] => terms } => sbody
                                 , ^^^.struct_tag => struct_tag
                                 }
                        , #VarDef{ #Rora{[] => terms} => var_type
                                 , new_id => var_name
                                 }
                        , #Stop{}
                        , (^^^.sbody, new_id) _resolve_struct_creation
                        , #Stop{}
                        , #Name{ new_id => name }
                        ]
                        #Rora{ ^ => terms}
                        #Paren{ ^ => pbody}
    : id != "" 
    & ^ #Assign 
    & ^.assignee.terms len == 0  
                   ?  #Assign{ #Name{ id => name } 
                               #Rora{ [^^] => terms }
                               => assignee
                             , #Attr{ ^^^.assigner.name => attr } => assigner
                             }
    : ^ #Rora      ?  ; ^.terms
                        [ (=>@, id) _resolve_struct_creation @=> @< ]
                        => ^.terms
                      ; ^
    : ^ #Paren     ?  ; (^^.pbody, "") _resolve_struct_creation
                      ; ^
    : ; ^ get_children [ (=>@, id) _resolve_struct_creation @<]
      ; ^
    )
)


Ast | run_semanalysis | () (
  ; ^ resolve_named_args
  ; ^ resolve_src
  ; ^ resolve_loopop
  ; ^ resolve_struct_creation
  ; ^ resolve_names
)
