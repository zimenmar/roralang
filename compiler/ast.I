Ast ({ Int: start_pos
     , Int: end_pos
     , Int: id
     , Type: type
     }
     & ( #BinOp{ String: op
               , Ast: l
               , Ast: r
               }
       | #LogOp{ Bool: is_and
               , [Ast]: ops
               }
       | #If{ Ast: cond
            , Ast: then
            , Ast: else
            }

       | #VarDef{ String: var_name
                , Ast: var_type
                , Type: inferred_var_type
                , Bool: is_global
                , Int: addr
                }
       | #StructDef{ String: struct_name
                   , Ast: struct_def
                   }
       | #RoraDef { String: rora_name
                  , [Ast]: args
                  , Ast: rv
                  , Ast: body
                  }

       | #Rora{ [Ast]: terms }
       | #Paren{ Ast: pbody }
       | #Loop{ Ast: lbody 
              , Type: in_type
              , Int: in_reg
              , Int: in_idx
              , Int: out_reg
              , Bool: is_generator
              }
       | #Struct{ Ast: sbody
                , String: struct_tag
                }
       | #Tuple{ [Ast]: tuplees }

       | #Fork{ Ast: fbody }
       | #Attr{ String: attr
              , Type: target_type
              }
       | #Index{ Ast: index
               , Type: src_type
               }
       | #Tag{ String: tagname }
       | #Assign{ Ast: assignee  
                , Ast: assigner
                }
       | #Ass{}

       | #Src{ Int: power }
       | #Lit{ Int: lit }
       | #BoolLit{ Int: bool_lit }
       | #Continue{ Ast: cloop, Int: cpower }
       | #Break{ Ast: bloop, Int: bpower }
       | #Pop{ Ast: poloop, Ast: break_target, Int: popower }
       | #Push{ Ast: puloop, Int: pupower }
       | #Stop{}
       | #Not{}
       | #Name{ String: name 
              , Ast: target
              }
       | #GetChar{ Ast: gloop}
       | #PutChar{}
       | #PutNum{}

       | #TypePlaceholder{ Type: placeholder }
       | #Failed{}
       | #Token{ Token: t }
       )
    )

Ast | ast_type_to_str | String
    ( ^ #BinOp           ? "BinOp"
    : ^ #LogOp           ? "LogOp"
    : ^ #If              ? "If"
    : ^ #VarDef          ? "VarDef"
    : ^ #StructDef       ? "StructDef"
    : ^ #RoraDef         ? "RoraDef"
    : ^ #Rora            ? "Rora"
    : ^ #Paren           ? "Paren"
    : ^ #Loop            ? "Loop"
    : ^ #Struct          ? "Struct"
    : ^ #Tuple           ? "Tuple"
    : ^ #Fork            ? "Fork"
    : ^ #Attr            ? "Attr"
    : ^ #Index           ? "Index"
    : ^ #Tag             ? "Tag"
    : ^ #Assign          ? "Assign"
    : ^ #Ass             ? "Ass"
    : ^ #Src             ? "Src"
    : ^ #Lit             ? "Lit"
    : ^ #BoolLit         ? "BoolLit"
    : ^ #Continue        ? "Continue"
    : ^ #Break           ? "Break"
    : ^ #Pop             ? "Pop"
    : ^ #Push            ? "Push"
    : ^ #Stop            ? "Stop"
    : ^ #Not             ? "Not"
    : ^ #Name            ? "Name"
    : ^ #GetChar         ? "GetChar"
    : ^ #PutChar         ? "PutChar"
    : ^ #PutNum          ? "PutNum"
    : ^ #TypePlaceholder ? "TypePlaceholder"
    : ^ #Failed          ? "Failed"
    : ^ #Token           ? "Token"
    :                      "UnknownAst"
    )

Ast | get_children | [Ast] 
    ( ^ #Rora      ?  ^.terms
    : ^ #Paren     ?  [^^.pbody]
    : ^ #Loop      ?  [^^.lbody]
    : ^ #Struct    ?  [^^.sbody]
    : ^ #If        ?  [^^.cond, ^^.then, ^^.else]
    : ^ #VarDef    ?  [^^.var_type]
    : ^ #RoraDef   ?  [^^.args, [^^^.rv, ^^^.body]] flatten
    : ^ #StructDef ?  [^^.struct_def]
    : ^ #Tuple     ?  ^.tuplees
    : ^ #LogOp     ?  ^.ops
    : ^ #BinOp     ?  [^^.l, ^^.r]
    : ^ #Fork      ?  [^^.fbody]
    : ^ #Index     ?  [^^.index]
    : ^ #Assign    ?  [^^.assignee, ^^.assigner]
    :                 []
    )

Ast | ast_label | String ( ^
    ( ^ #Token    ?  ^.t.str escape_quotes
    : ^ #Struct   ?  ^.struct_tag ["#", ^] flatten
    : ^ #RoraDef  ?  ^.rora_name
    : ^ #BinOp    ?  ^.op
    : ^ #Name     ?  ^.name
    : ^ #VarDef   ?  ["VarDef", ^^.id int_to_str, "\n", ^^.var_name] flatten
    : [^^ ast_type_to_str, ^^.id int_to_str] flatten
    )
    [^, "\n", ^^.type type_to_str ] flatten
)

Ast | id_ast | () (
  ; 0 => cur_id;
  ; ^ _id_ast
)

Ast: ast | _id_ast | () (
    ; next_id => ast.id
    ; ast get_children [ =>@ _id_ast @< ]
)

Ast:ast | ast_dot_helper | String (
    ; ast get_children [ =>@
       Y([ "\t" 
         , ast ast_type_to_str
         , ast.id int_to_str 
         , " -> "
         , ^^ ast_type_to_str
         , ^^.id int_to_str
         , ";\n"
         ] flatten @=>)
       ast_dot_helper @=>
       @< 
       ] 
       [ [ "\t"
         , ast ast_type_to_str
         , ast.id int_to_str
         , " [label=\""
         , ast ast_label
         , "\"];\n"
         ]
       , ^
       ]
       flatten flatten
)

Ast | ast_dot | String (
  ; ^ id_ast
  ; ["digraph G {\n", ^^ ast_dot_helper, "}"] flatten
)
