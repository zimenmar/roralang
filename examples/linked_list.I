List( #Nil{}
    | #Cons{ Int: car
           , List: cdr
           }
    )

List: l, Int: x | append | List (
    #Cons{ x => car
         ; l => cdr
         }
    )

List: l, Int: x | count | Int ( l #Nil     ? 0
                              : l.car == x ? (l.cdr, x) count + 1
                              :              (l.cdr, x) count
                              )

[String] | main | Int (
    #Nil{}
    (^, 1) append
    (^, 2) append
    (^, 2) append
    (^, 3) append
    (^, 1) append
    (^, 1) append
    (^, 1) count
    putnum
)
