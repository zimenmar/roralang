cat compiler/*.I > compiler.I
echo "Compiling..." >&2
python bootstrap.py --stdin $1 compiler.I > out.mixed
echo "Preparing files..." >&2
cat <<EOF | python
with open("out.mixed", "r") as mixed\
   , open("out.dot", "w")   as dot\
   , open("out.t86", "w")   as t86:
        out = dot
        for line in mixed.readlines():
            if line.strip() == "=====":
                out = t86
                continue
            out.write(line)
EOF
echo "Producing AST..." >&2
dot out.dot -Tpdf > out.pdf
echo "Running t86-cli..." >&2
~/t86-with-debug/src/build/t86-cli/t86-cli --register-cnt 200 --memory-size 1000000 out.t86
                
