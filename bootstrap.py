import lark
from dataclasses import dataclass
from typing import Any
from contextlib import contextmanager
import sys
import types
import builtins as pybuiltins
from pprint import pprint

sys.setrecursionlimit(1_000_000)

import logging
lark.logger.setLevel(logging.DEBUG)

def error(*args, **kwargs):
    print(*args, **kwargs, file=sys.stderr)

parser = lark.Lark(r"""
    start: ((roradef | structdef | globaldef) ";"?)*
    roradef: type_name_opt ("," type_name_opt)* "|" ID "|" type term
    globaldef: type_name

    structdef: TYPENAME struct_tag
    ?struct_or: struct_and ("|" struct_and)*
    ?struct_and: struct_tag ("&" struct_tag)*
    ?struct_tag: struct_def | "#" TYPENAME struct_def -> struct_tag
    ?struct_def: "(" struct_or ")" | "{" "}" | "{" type_name ("," type_name)* ","? "}" -> struct_def

    ?type_name_opt: type (":" ID)?
    type_name: type ":" ID
    ?type: TYPENAME -> type_name
         | "[" type "]" -> type_list
         | type "->" type -> type_fun
         | "(" type ("," type)+ ","? ")" -> type_tuple
         | "(" type "," ")" -> type_tuple
         | "(" ")" -> type_tuple

    assign_pure: (ID|SRC) ( ("." ID) | ("$" indexer) )*
    indexer: ID | NUM | "(" _start ")"

    _start: if
    ?if: tuple | tuple "?" tuple ":" if
    ?tuple: assign | assign "," -> tuple | assign ("," assign)+ ","?
    ?assign: or | assign ("=>" (type_name | assign_pure))+ rora?
    ?or: and ("|" and)*
    ?and: comp ("&" comp)*
    ?comp: add | add compop add
    !compop: "==" | "!=" | "<" | ">" | ">=" | "=<"
    ?add: mul | add addop mul
    !addop: "+" | "-"
    ?mul: rora | mul mulop rora
    !mulop: "*" | "/" | "%"
    rora: term+
    ?term: ID -> var
        | STR -> str
        | (NUM | CHAR) -> const
        | "Y" "(" _start ")" -> fork
        | "(" _start? ")" -> paren
        | "." ID -> attr
        | "!" -> neg
        | SRC -> src
        | "$" indexer -> index
        | "\\" ID  -> funptr
        | LOOPOP -> loopop
        | ";" -> stop
        | "[" _start? "]" -> loop
        | "#" TYPENAME -> tag
        | "{" _start? "}" -> struct
        | "#" TYPENAME "{" _start? "}" -> tagged_struct

    TYPENAME: /[A-Z][a-zA-Z_]*/
    ID: /[a-z_][a-zA-Z_]*/
    NUM: /[0-9]+/
    STR: /"([^\\\\"]|\\\\"|\\[nt]|\\\\)*"/
    CHAR: /'.'/
    SRC: /\^+/
    LOOPOP: /@+</ | /@+>/ | /=>@+/ | /@+=>/

    COMMENT: "//" /.*/
    %import common.WS
    %ignore WS
    %ignore COMMENT
    """,
    parser="lalr",
    lexer="basic",
    propagate_positions=True,
    debug=True)

def get_loc(t):
    if type(t) == lark.Token:
        return t.line, t.end_line, t.column, t.end_column
    data = [loc for loc in map(get_loc, t.children) if loc is not None]
    if len(data) == 0:
        return None
    return data[0][0], data[-1][1], data[0][2], data[-1][3]

def print_loc(tree):
    line, end_line, column, end_column = get_loc(tree)
    lines = program.split("\n")
    return "\n".join((
        f"Line: {line}, Column {column}",
        column * " " + " v",
        "  " + lines[line - 2],
        *("> " + lines[i] for i in range(line - 1, end_line)),
        "  " + lines[end_line],
        end_column * " " + "^",
        f"Line: {end_line}, Column {end_column}"))

class InterpreterError(Exception):
    def __init__(self, loc, message):
        if loc is None:
            super().__init__(message + "\n" + "unknown location")
        else:
            super().__init__(message + "\n" + print_loc(loc))

def with_located_errors(message):
    def decorator(f):
        def eval(c, *args, **kwargs):
            try:
                return f(c, *args, **kwargs)
            except (Break, Continue, InterpreterError): raise
            except Exception as e:
                raise InterpreterError(c, message()) from e
        return eval
    return decorator

@dataclass(frozen=True)
class Rora:
    name: str
    arg_names: list[str]
    body: lark.tree.Tree

@dataclass(frozen=True)
class NatFun:
    f: types.FunctionType

@dataclass(frozen=True)
class Int:
    val: int

@dataclass(frozen=True)
class Bool:
    val: bool

@dataclass(frozen=True)
class Struct:
    vals: dict

@dataclass(frozen=True)
class List:
    vals: list

@dataclass(frozen=True)
class Tuple:
    vals: list
    is_open: bool

@dataclass
class Loop:
    src: list
    src_idx: int
    dst: list

@dataclass
class Break(Exception):
    power: int
    val: Any

@dataclass
class Continue(Exception):
    power: int

none = Tuple([], False)
class n: ne = none # For use in match-case `case n.ne:`

class Builtins:
    src = sys.stdin
    def stdin(_):
        try:
            return Int(ord(Builtins.src.read(1)))
        except:
            raise Break(1, none)
    def stdout(x):
        assert isinstance(x, Int), f"Cannot stdout '{x}'"
        print(chr(x.val), end="", file=sys.stdout)
    def stderr(x):
        assert isinstance(x, Int), f"Cannot stderr '{x}'"
        print(chr(x.val), end="", file=sys.stderr)

class JIT:
    def flatten(ll):
        assert isinstance(ll, List)
        return List([x for l in ll.vals for x in l.vals])
    def sort_by_key(x):
        assert isinstance(x, List)
        x.vals.sort(key=lambda t: t.vals[0].val, reverse=True)
        return List([t.vals[1] for t in x.vals])
    def dedup(l):
        assert isinstance(l, List)
        return List(list(set(l.vals)))

globals = {}
roras = {}
builtins = {
        "true": Bool(True),
        "false": Bool(False),
        "debug": NatFun(lambda x: pprint(x, stream=sys.stderr, depth=4)),
        "putnum": NatFun(lambda x: print(int(x.val))),
        "stdin": NatFun(Builtins.stdin),
        "stdout": NatFun(Builtins.stdout),
        "stderr": NatFun(Builtins.stderr)
        }

def process_string(s):
    return List([Int(ord(c)) for c in pybuiltins.eval(s)])

def process_const(c):
    match c.type:
        case "NUM": return Int(int(c))
        case "CHAR": return Int(ord(c[1]))
        case unknown: InterpreterError(c, f"Unknown consts {c}")

def boolify(x):
    match x:
        case Bool(b): return b
        case unknown: raise TypeError(f"Unknown boolify {unknown}")

def is_loop_generator(tree, depth = 1):
    if isinstance(tree, lark.Token): return False
    match tree.data, tree.children:
        case "loop", [body]:
            return is_loop_generator(body, depth + 1)
        case "loopop", [op]:
            if op[-2:] != "=>": return False
            power = sum(1 for c in op if c == "@")
            return depth == power
        case _, children:
            return any(is_loop_generator(c, depth) for c in children)

def run_rora(rora, input):

    locals = {}

    def standard_assign(name, value):
        if name is None: return
        if name in globals:
            globals[name] = value
        else:
            locals[name] = value
    assign = standard_assign

    @contextmanager
    def escape_struct_assign():
        nonlocal assign
        old_assign = assign
        assign = standard_assign
        try:
            yield
        finally:
            assign = old_assign

    def lookup(name):
        if hasattr(JIT, name): return NatFun(getattr(JIT, name))
        return (builtins | roras | globals | locals)[name]

    src_stack = [none]

    @contextmanager
    def new_src(src):
        src_stack[-1] = src
        src_stack.append(none)
        try:
            yield
        finally:
            src_stack.pop()

    if len(rora.arg_names) == 1:
        assign(rora.arg_names[0], input)
    else:
        if not isinstance(input, Tuple) or len(rora.arg_names) != len(input.vals):
            raise TypeError(f"Argument mismatch when calling {rora.name}")
        for name, val in zip(rora.arg_names, input.vals):
            assign(name, val)

    def eval_rora(body, the = none):
        def apply(new):
            nonlocal the
            match new:
                case NatFun(f): the = f(the)
                case Rora() as r: the = run_rora(r, the)
                case x: the = x

        @with_located_errors(lambda: f"Error in rora element. the = {the}")
        def eval_rora_elem(c):
            nonlocal the
            match c.data, c.children:
                case "stop", []: apply(none)
                case "const", [val]: apply(process_const(val))
                case "str", [raw]: apply(process_string(raw))
                case "neg", []: apply(Bool(not boolify(the)))
                case "src", [op]: apply(src_stack[ -len(op)-1 ])
                case "var", [name]: apply(lookup(name))
                case "funptr", [name]: the = lookup(name)
                case "paren", []: apply(none)
                case "paren", [body]:
                    with new_src(the), escape_struct_assign():
                        match eval(body):
                            case Tuple(t, True): evaled = Tuple(t, False)
                            case x: evaled = x
                        apply(evaled)
                case "fork", [body]:
                    with new_src(the), escape_struct_assign():
                        eval(body)
                case "loop", []:
                    apply(List([]))
                case "loop", [body]:
                    with new_src(the):
                        apply(eval_loop(body, getattr(the, "vals", [])))
                case "loopop", [op]:
                    power = sum(1 for c in op if c == "@")
                    if op[:2] == "=>":
                        l = loop_stack[-power]
                        i = l.src_idx
                        if i == len(l.src): raise Break(1, none)
                        l.src_idx += 1
                        apply(l.src[i])
                    elif op[-2:] == "=>":
                        loop_stack[-power].dst.append(the)
                    elif op[-1:] == ">": raise Break(power, the)
                    elif op[-1:] == "<": raise Continue(power)
                    else: raise Exception("Unreachable")
                case "attr", [name]:
                    assert isinstance(the, Struct)
                    apply(the.vals[name])
                case "index", [indexer]:
                    assert isinstance(the, List) or isinstance(the, Tuple)
                    with new_src(the):
                        apply(the.vals[eval_indexer(indexer)])
                case ("tagged_struct" | "struct"), body:
                    struct = Struct({})
                    nonlocal assign
                    old_assign = assign
                    def assign(name, value):
                        struct.vals[name] = value
                    for b in body:
                        if isinstance(b, lark.Token):
                            struct.vals["tag"] = Int(hash(b)) # Allows Int ops
                            struct.vals["_tag"] = b # Shows in debug prints
                            struct.vals["tag_string"] = process_string(f'"{b}"')
                        else:
                            with new_src(the):
                                eval(b)
                    assign = old_assign
                    apply(struct)
                case "tag", [tagname]:
                    assert isinstance(the, Struct)
                    apply(Bool(the.vals["tag"].val == hash(tagname)))
                case unknown:
                    raise Exception("Unreachable")
        for c in body:
            eval_rora_elem(c)
        return the

    loop_stack = []
    def eval_loop(body, src):
        loop = Loop(src, 0, [])
        loop_stack.append(loop)
        while True:
            try:
                match eval(body):
                    case Tuple(l, True): rv = List(l)
                    case n.ne: rv = none
                    case x: rv = List([x])
                break
            except Break as b:
                if b.power > 1:
                    b.power -= 1
                    raise b
                rv = b.val
                break
            except Continue as c :
                if c.power > 1:
                    c.power -= 1
                    raise c
                continue
        loop_stack.pop()
        if is_loop_generator(body): return List(loop.dst)
        return rv

    def eval_indexer(indexer):
        indexer = indexer.children[0]
        match indexer:
            case lark.Token("ID", name): index = lookup(name)
            case lark.Token("NUM", _): index = process_const(indexer)
            case lark.tree.Tree(): index = eval(indexer)
        assert isinstance(index, Int), str(index)
        return index.val

    @with_located_errors(lambda: "Error in tree structure")
    def eval(tree):
        match tree.data, tree.children:
            case "rora", body:
                return eval_rora(body)
            case "if", [cond, then_body, else_body]:
                if boolify(eval(cond)):
                    return eval(then_body)
                else:
                    return eval(else_body)
            case "tuple", vals:
                return Tuple([eval(v) for v in vals], True)
            case "assign", [val, *dst]:
                val = eval(val)
                for d in dst:
                    match d.data, d.children:
                        case "rora", body: return eval_rora(body, val)
                        case "type_name", [_, name]: assign(name, val)
                        case "assign_pure", [name]: assign(name, val)
                        case "assign_pure", [name, *ops, last]:
                            match name:
                                case lark.Token("ID", name): target = lookup(name)
                                case lark.Token("SRC", src): target = src_stack[ -len(src)-1 ]
                            for op in ops:
                                match op:
                                    case lark.Token("ID", attr):
                                        assert isinstance(target, Struct), str(target)
                                        target = target.vals[attr]
                                    case lark.tree.Tree(data="indexer", children=_) as indexer:
                                        assert isinstance(target, List), str(target)
                                        with new_src(val):
                                            target = target.vals[eval_indexer(indexer)]
                            match last:
                                case lark.Token("ID", attr):
                                    assert isinstance(target, Struct), str(target)
                                    target.vals[attr] = val
                                case lark.tree.Tree(data="indexer", children=_) as indexer:
                                    assert isinstance(target, List), str(target)
                                    with new_src(target):
                                        target.vals[eval_indexer(indexer)] = val
                return val
            case ("or" | "and") as op, args:
                if op == "or": toggle = lambda x: x
                if op == "and": toggle = lambda x: not x
                for a in args:
                    a = eval(a)
                    if toggle(boolify(a)): return a
                return a
            case ("comp" | "add" | "mul"), [l, op, r]:
                l, r = eval(l), eval(r)
                op = op.children[0]
                if op == "==": return Bool(l == r)
                if op == "!=": return Bool(l != r)
                assert isinstance(l, Int), str(l)
                assert isinstance(r, Int), str(r)
                l, r = l.val, r.val
                if op == "<": return Bool(l < r)
                if op == ">": return Bool(l > r)
                if op == ">=": return Bool(l >= r)
                if op == "=<": return Bool(l <= r)
                if op == "+": return Int(l + r)
                if op == "-": return Int(l - r)
                if op == "*": return Int(l * r)
                if op == "/": return Int(l // r)
                if op == "%": return Int(l % r)
                raise Exception()
            case unknown:
                raise InterpreterError(tree, f"Unknown AST node {unknown}")

    return eval_rora([rora.body], input)

def load_toplevel(tree):
    assert tree.data == "start"
    for c in tree.children:
        match c.data, c.children:
            case "roradef", [*arg_types, name, _, body]:
                arg_names = [
                        x.children[1] if x.data == "type_name_opt" else None
                        for x in arg_types]
                roras[name] = Rora(name, arg_names, body)
            case "globaldef", [typename]:
                globals[typename.children[1]] = none

if __name__ == "__main__":
    argi = 1
    if sys.argv[argi] == "--stdin":
        Builtins.src = open(sys.argv[argi+1])
        argi += 2
    program = open(sys.argv[argi]).read()
    argi += 1
    try:
        tree = parser.parse(program)
    except lark.LarkError as e:
        print(e)
        lines = program.split("\n")
        for line in lines[e.line - 4: e.line-1]:
            print(" ", line)
        print(">", lines[e.line-1])
        print(" ", lines[e.line])
        print(e.column * " " + " ^")
        exit(1)

    #lark.tree.pydot__tree_to_png(tree, "ast.png")
    load_toplevel(tree)
    run_rora(roras["main"], List([process_string(a) for a in sys.argv[argi:]]))
