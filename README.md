# The Róra programming language

Róra is the Haná dialect for "roura" (Czech for pipe). It's called this way because we don't use the pipe (`|`) operator.

## Project info

This was a university course project I've now passed. If I find an excuse to continue working on it, I gladly will.

* There was quite a lot of effort put into designing the language (compared to the intended effort in the course and it is not a language design course)
    * I'm most proud that I discovered that Brujn indexes and ternary operators produce quite powerful "pattern matching"
* There is a working interpreter of Rora. It ignores all type information and struct declaration.
* There is a Rora compiler written in Rora. 
    * It has terrible error messages as all input programs were already debugged by the interpreter.
    * The typechecker is also terrible. It basically assumes the program is correct and only propagates the type so polymorphic operators and struct attributes offsets can be resolved.
    * It has a custom lexer and parser. There is no reason other than that I've always wanted to try figure out parsing out on my own. It doesn't even try to produce the same AST as the interpreter parser.
        * It works quite well 
    * It does regalloc. It uses liveness analysis and top-down approach.
    * It produces machine code in `t86` -- a toy ISA used in the course.
* There are code examples (including the compiler itself)
    * Unfortunately, they are not explained
    * The features of the language are quite overused
    * In the compiler, there is quite notable, how the language evolved and the code style changed. I find that quite interesting.

There is still a lot of interesting topics to explore.

* I'd like to add `\` operator which would create a function from the escaped piece of code. Basically lambdas. (e.g. `[ \=>@ @> ]` would result in an function that could be called repeatedly called to yield an element from list). This was not done because I didn't want to implement closure in native code.
* I'd like to add the option lookahead when working with `=>@`. Probably `@.1`. This would simplify a lot of code in the lexer and parser.
* Allow currying
* Unite `$` and `.`. This would leave `$` for changing the direction of evaluation (e.g. `list append$x` (assuming `append` is curried)
* I'd like to compile to Python bytecode. I have quite close to Python and it would be quite a fit for a higher language.

## Language description

### Left-right direction

Multiple terms next to each other form a róra. A value is passed and modified along the róra. It's like Bash without the pipe (`|`) symbol.

* Functions are applied this way. Every function has only one argument and one return value. Mathematical expression `sin(cos(x))` would be written as `x cos sin`.
* The fork operator `Y` evaluates the inner róra but ignores the return value and keeps the original value.
* Operator consisting of $n$ hat (`^`) characters refers to the value passed into the n-th outer parenthesis, brackets or square brackets. E.g. `3 (42 / ^)` evaluates to 14. Or `42 (^ + 1, (^^ + 1)*3)` evaluates to tuple `(43, 129)`.
* Assignment is done using `=> E`. It saves the current value in róra to variable `E`. `E` may contain type annotation `Int: x` but the interpreter ignores it.

### Loop

The brackets create a loop (`[ body ]`). There are 4 operators to manipulate the loop.

* `@<` acts as "continue"
* `@>` acts as "break" (argument can be passed onto it which acts as return from the loop)
* `=>@` reads the next element from the list this loop was applied to (e.g. `l [=>@ print; @<]` prints all elements of `l`)
* `@=>` adds it's input into list that is returned from the loop

When `@=>` isn't used, the evaluated body is wrapped into the return list (e.g. `[4]` or `[2, 3, 4]`)

When the `@` symbol is repeated, it refers to the outer loop(s).

### Struct

The are tagged structs. The interpreter ignores the declaration and treats structs as a Python object (i.e. a dictionary). Every struct has a tag. Struct is created with `#Tag{ 42 => x; "ahoj" => y}` This creates struct with tag `Tag` and two fields `x` and `y` set to respective values. 

The tag may be tested with `#Tag`. Think of it as a function that returns `true` if the input struct has tag `Tag`.

### Misc

* Chars are numbers, strings are list of numbers
    * `'a' == 97`
    * `"abc" == [97, 98, 99]`
* There are `stdin` and `stdout` functions
* See examples `fac`, `linked_list` and `eratostenes`
