REV="HEAD~"
[ $# -eq 1 ] && REV="$1"

PREV=$(mktemp)
PREV_EXAMPLE=$(mktemp)
git show $REV:bootstrap.py > $PREV

echo > cur.out
echo > prev.out
for EXAMPLE in examples/*.I
do
    git show $REV:$EXAMPLE > $PREV_EXAMPLE
    echo $EXAMPLE >> cur.out
    echo $EXAMPLE >> prev.out
    python bootstrap.py $EXAMPLE &>> cur.out
    python $PREV $PREV_EXAMPLE &>> prev.out
done

diff cur.out prev.out
