for EXAMPLE in examples/*.I
do
    echo $EXAMPLE
    python bootstrap.py $EXAMPLE 2>/dev/null > interpret.out
    bash compile.sh $EXAMPLE 2>/dev/null > compiler.out
    diff interpret.out compiler.out
done
